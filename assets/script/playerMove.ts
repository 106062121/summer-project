// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import {gameInit} from "./gameInit"
import {demoCombo} from "./demoCombo"

const {ccclass, property} = cc._decorator;

enum keyDir {
    up = 0,
    down,
    left,
    right
}

@ccclass
export default class playerMove extends cc.Component {
    private body = null;
    private bulletPool = null;

    @property(demoCombo)
    demo: demoCombo;

    @property(gameInit)
    game: gameInit;

    @property(cc.Prefab)
    private bulletPrefab: cc.Prefab = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        //cc.director.getPhysicsManager().gravity = cc.v2();

        var manager = cc.director.getCollisionManager();
	    manager.enabled = true;

        this.body = this.node.getComponent(cc.RigidBody);

        this.bulletPool = this.game.bulletPool;
    }

    start () {
        cc.log('start');

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyUp, this);
    }

    // update (dt) {}

    // CUSTOM FUNCTIONS:

    onKeyDown (event) {     
        switch (event.keyCode) {
            case cc.macro.KEY.up:
                this.node.runAction(cc.moveTo(0.2, cc.v2(this.node.x, this.node.y + 50)).easing(cc.easeElasticOut(3.0)));
                //this.body.applyLinearImpulse(cc.v2(0, 6000), this.body.getWorldCenter());
                break;
            case cc.macro.KEY.down:
                this.node.runAction(cc.moveTo(0.2, cc.v2(this.node.x, this.node.y - 50)).easing(cc.easeElasticOut(3.0)));
                //this.body.applyLinearImpulse(cc.v2(0, -6000), this.body.getWorldCenter());
                break;
            case cc.macro.KEY.left:
                this.node.runAction(cc.moveTo(0.2, cc.v2(this.node.x - 50, this.node.y)).easing(cc.easeElasticOut(3.0)));
                //this.body.applyLinearImpulse(cc.v2(-6000, 0), this.body.getWorldCenter());
                break;
            case cc.macro.KEY.right:
                this.node.runAction(cc.moveTo(0.2, cc.v2(this.node.x + 50, this.node.y)).easing(cc.easeElasticOut(3.0)));
                //this.body.applyLinearImpulse(cc.v2(6000, 0), this.body.getWorldCenter());
                break;
        }
    }

    onKeyUp (event) {
        let status: number = this.demo.status();
        if (status === this.demo.comboLength) {
            cc.log('KA ME HA ME HO!!!!!!!!');
            this.playerAttack(0, 0);
            this.playerAttack(0, 10);
            this.playerAttack(0, -10);
            this.playerAttack(0, 20);
            this.playerAttack(0, -20);
        }
    }

    onBeginContact (contact, self, other) {
        cc.log('Player Collision!');
        this.playerDie();
    }

    playerDie () {
        cc.log('You Die!');
    }

    playerAttack (x: number, y: number) {
        let bullet = null;

        if (this.bulletPool.size() > 0) {
            bullet = this.bulletPool.get(this.bulletPool); // call the reuse function (the parameter can also be altered)
        }
        else {
            bullet = cc.instantiate(this.bulletPrefab);
            this.bulletPool.put(bullet);
            bullet = this.bulletPool.get(this.bulletPool);
        }   

        if (bullet != null) {
            bullet.getComponent('bullets').init(this.node.x + x, this.node.y + y, this.node); // custion init function in bullet.ts
        }
    }
    
}
