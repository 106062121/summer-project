// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class obstacles extends cc.Component {

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;

        var manager = cc.director.getCollisionManager();
	    manager.enabled = true;
    }

    start () {
        cc.log('start');
    }

    // update (dt) {}

    onBeginContact (contact, self, other) {
        cc.log('Obstacle Collision!');
        this.node.destroy();
    }
}
