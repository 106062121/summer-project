// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

enum keyDir {
    up = 0,
    down,
    left,
    right
}

@ccclass
export class demoCombo extends cc.Component {
    private comboIndex: number = 0;
    private comboArray: number[] = [keyDir.up, keyDir.up, keyDir.down, keyDir.down, keyDir.left, keyDir.right, keyDir.left, keyDir.right];
    private validArray: number[] = [0, 0, 0, 0, 0, 0, 0, 0]; 

    public comboLength: number = 8;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    // start () {}

    // update (dt) {}

    // CUSTOM FUNCTIONS:

    input (key) {
        if (this.comboIndex === this.comboArray.length) {
            this.comboIndex = 0;
        } 
        
        if (key === this.comboArray[this.comboIndex]) {
            this.validArray[this.comboIndex] = 1;
            this.comboIndex += 1;
        }
        else {
            this.validArray.fill(0);
            this.comboIndex = 0;

            if (key === this.comboArray[this.comboIndex]) {
                this.validArray[this.comboIndex] = 1;
                this.comboIndex += 1;
            }
        }
    }

    status () {
        return this.comboIndex;
    }
}
