// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class bullets extends cc.Component {
    private body = null;
    private bulletPool = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().gravity = cc.v2();

        var manager = cc.director.getCollisionManager();
	    manager.enabled = true;

        this.body = this.node.getComponent(cc.RigidBody);
    }

    start () {
        
    }

    // update (dt) {}

    reuse(bulletPool){
        this.bulletPool = bulletPool;
    }

    onBeginContact (contact, self, other) {
        if (other.node.name === 'obstacle') {
            cc.log('Bullet Collision!' + other.node.name);
            // this.node.destroy();
            this.bulletPool.put(this.node);
        }
    }

    init (x: number, y: number, node: cc.Node) {
        this.node.parent = node.parent;
        this.node.setPosition(x - 30, y);
        this.body.applyLinearImpulse(cc.v2(-600, 0), this.body.getWorldCenter());
    }
}
