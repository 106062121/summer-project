// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
import {demoCombo} from "./demoCombo"

const {ccclass, property} = cc._decorator;

enum keyDir {
    up = 0,
    down,
    left,
    right
}

@ccclass
export default class demoKeyControl extends cc.Component {
    private keys = null;
    private anims = [];
    
    @property(demoCombo)
    demo: demoCombo;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.keys = this.node.children;
        for (let key of this.keys) {
            this.anims.push(key.getComponent(cc.Animation));
        }
    }

    start () {
        cc.log('start');

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyUp, this);
    }

    // update (dt) {}

    // CUSTOM FUNCTIONS:

    onKeyDown (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.up:
                this.demo.input(keyDir.up);
                break;
            case cc.macro.KEY.down:
                this.demo.input(keyDir.down);
                break;
            case cc.macro.KEY.left:
                this.demo.input(keyDir.left);
                break;
            case cc.macro.KEY.right:
                this.demo.input(keyDir.right);
                break;
        }
    }

    onKeyUp (event) {
        for (let i = 0; i < this.demo.comboLength; i++) {
            var keyType = this.keys[i].name;
            this.anims[i].play(keyType + '_clear');
        }

        let status: number = this.demo.status();
        if (status) {
            for (let i = 0; i < status; i++) {
                var keyType = this.keys[i].name;
                this.anims[i].play(keyType + '_active');
            }
        }

        if (status === this.demo.comboLength) {
            for (let i = 0; i < status; i++) {
                var keyType = this.keys[i].name;
                var animState = this.anims[i].play(keyType + '_active');
                animState.repeatCount = 20;
            }
        }
    }
}
